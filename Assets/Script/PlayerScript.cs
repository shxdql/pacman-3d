﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public float speed;
    public Text counttext;
    public Text wintext;
    public GameObject panel;
    public GameObject countgo;

    private Rigidbody rb;
    private int count;

    void Start()
    {
        panel.SetActive(true);
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetText();
        wintext.text = "";
    }

    void FixedUpdate()
    {
        float hori = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(hori, 0.0f, vert);

        rb.AddForce(move * speed);

        CheckWin();

        GameOver();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Cube"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetText();
        }

        if(other.gameObject.CompareTag("Enemy"))
        {
            wintext.text = "LOSE";
        }
    }

    void SetText()
    {
        counttext.text = "Skor: " + count.ToString();
    }

    void CheckWin()
    {
        if(count == 10)
        {
            wintext.text = "WIN";
        }
    }

    void GameOver()
    {
        if (wintext.text == "WIN" || wintext.text == "LOSE")
        {
            panel.SetActive(true);
            countgo.SetActive(false);
        }
    }
}
