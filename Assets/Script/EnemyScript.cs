﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    public Transform target;
    public Text wintext;

    private NavMeshAgent nav;

    void Start()
    {
        target = GetComponent<Transform>();
        nav = this.gameObject.GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if(wintext.text != "LOSE" && wintext.text != "WIN")
        {
            if (target)
                nav.SetDestination(target.position);
            else
                target = GetComponent<Transform>();
        }
        else
        {
            target = GetComponent<Transform>();
        }
    }
}
